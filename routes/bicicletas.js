var express = require('express');
var router = express.Router();
var controllerBicicleta = require('../controllers/controller_bicicleta');

/* GET home page. */
router.get( '/all', controllerBicicleta.bicicleta_list );
router.get( '/create', controllerBicicleta.bicicleta_create_get);
router.post( '/create', controllerBicicleta.bicicleta_create_post);
router.post('/delete/:id', controllerBicicleta.bicicleta_remove_delete)

module.exports = router;