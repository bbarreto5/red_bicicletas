var express = require('express');
var router = express.Router();
var controllerBicicleta = require('../../controllers/api/v1/controller_bicicleta_api');

/* api v1 bicicleta */
router.get( '/bicicleta/all', controllerBicicleta.bicicleta_list );
/*router.get( '/create', controllerBicicleta.bicicleta_create_get);
router.post( '/create', controllerBicicleta.bicicleta_create_post);
router.post('/delete/:id', controllerBicicleta.bicicleta_remove_delete)*/

module.exports = router; 