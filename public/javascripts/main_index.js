var mymap = L.map('main_map').setView([51.505, -0.09], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(mymap);

L.control.scale().addTo(mymap);

L.marker([51.505, -0.09],{draggable: true}).addTo(mymap);

var req = new XMLHttpRequest();
req.responseType = 'json';
req.open('GET', '/api/v1/bicicleta/all', true);
req.onload = ( data ) => {
    let responseObj = req.response.lista;
    responseObj.forEach(element => {
        L.marker(element.ubicacion,{draggable: true}).addTo(mymap);
    });
}
req.send(null);