var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');
var mongoose = require('mongoose');

describe('Testing model usuario', function () {
    
    beforeEach((done)=>{
        mongoose.connect('mongodb://127.0.01:27017/test', {useNewUrlParser: true});
        
        mongoose.Promise = global.Promise;

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log("successfull");
            done();
        });
    });

    afterEach((done)=>{
        Bicicleta.deleteMany({},(error)=>{
            if(error) console.log(error);
            done();
        });
        Usuario.deleteMany({},(error)=>{
            if(error) console.log(error);
            done();
        });
        Reserva.deleteMany({},(error)=>{
            if(error) console.log(error);
            done();
        });
    });

    it('reservar', function (done) {
        let nUsuario =  new Usuario({
            nombre:'brandon'
        });
        nUsuario.save();
        let nBicicleta = new Bicicleta({
            codigo:1,
            modelo:"urbano",
            marca:"kaloy",
            color:"rojo",
            ubicacion:[51.500, -0.09]
        });
        nBicicleta.save();
        let hoy = new Date();
        let mañana = new Date().setDate(hoy.getDate() + 1);
        nUsuario.reservar(nBicicleta._id, hoy, mañana, function (err, success) {
            if(err) console.log(err);
            Reserva.find({}).populate('Bicicleta').populate('Usuario').exec(function (err, lista) {
                if(err) console.log(err);
                expect(lista.length).toBe(1);
                expect(lista[0].bicicleta._id).toEqual(nBicicleta._id);
                expect(lista[0].usuario._id).toEqual(nUsuario._id);
                done();
            });    
        });
        
    });
});