var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');

describe("Testing model Bicicleta", function() {
    beforeEach((done)=>{
        mongoose.connect('mongodb://127.0.01:27017/test', {useNewUrlParser: true});
        
        mongoose.Promise = global.Promise;

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log("successfull");
            done();
        });
    })

    afterEach((done)=>{
        Bicicleta.deleteMany({},(error)=>{
            if(error) console.log(error);
            done();
        })
    });

    it("instanciar bicicleta", ()=>{
        let a = Bicicleta.createInstance(1, "urbano", "kaloy", "rojo", [51.500, -0.09]);
        //(2, "urbano", "kaloy", "verde", [51.505, -0.08])

        expect(a.codigo).toBe(1);
        expect(a.marca).toBe("kaloy");
        expect(a.modelo).toBe("urbano");
        expect(a.color).toBe('rojo');
        expect(a.ubicacion[0]).toEqual(51.500);
        expect(a.ubicacion[1]).toEqual(-0.09);
    });
    
    it("documento vacio", ( done )=>{
        Bicicleta.allBici(function ( error, lista_bici) {
            if(error) console.log(error);
            expect(lista_bici.length).toBe(0);
            done();
        });
    });
    
    it("agregar bicicleta", (done)=>{
        let a = new Bicicleta({codigo:1, modelo:"urbano", marca:"kaloy", color:"rojo", ubicacion:[51.500, -0.09]});
        //(2, "urbano", "kaloy", "verde", [51.505, -0.08])

        Bicicleta.add( a, function ( error, newBici)  {
            if(error) console.log(error);
            Bicicleta.allBici(function ( error, lista_bici ) {
                if(error) console.log(error);
                expect(lista_bici.length).toBe(1);
                expect(lista_bici[0].codigo).toBe(1);
                done();
            })
        });
    });
});