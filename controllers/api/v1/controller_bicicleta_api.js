var Bicicleta = require('../../../models/bicicleta');

exports.bicicleta_list = function ( req,  res){
    res.json({ lista: Bicicleta.allBicicletas });
}