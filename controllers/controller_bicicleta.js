var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function ( req,  res){
    res.render('bicicletas/index', { lista: Bicicleta.allBicicletas })
}

exports.bicicleta_create_get = function( req, res){
    res.render('bicicletas/create')
}

exports.bicicleta_create_post = function( req, res){
    let { id, modelo, marca, color, latitud, longitud } = req.body;
    Bicicleta.add(new Bicicleta(id, modelo, marca, color, [latitud, longitud]));
    res.redirect('/bicicleta/all');
}

exports.bicicleta_remove_delete = function( req, res){
    Bicicleta.deleteById(req.params.id);    
    res.redirect('/bicicleta/all');
}