const mongoose = require('mongoose');
const schema = mongoose.Schema;
const moment = require('moment');

var ReservaSchema = new schema({
    desde: Date,
    hasta: Date,
    bicicleta:{
        type: mongoose.Schema.Types.ObjectId, ref: 'bicicleta'
    },
    usuario:{
        type: mongoose.Schema.Types.ObjectId, ref: 'usuario'
    }
});

module.exports = mongoose.model('Reserva', ReservaSchema);
