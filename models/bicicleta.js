const mongoose = require('mongoose');
const schema = mongoose.Schema;

var BicicletaSchema = new schema({
    codigo: Number,
    modelo: String,
    marca: String,
    color: String,
    ubicacion:{
        type: [Number], index:{ type:'2dsphere', sparse:true }
    }
});

BicicletaSchema.methods.toString = function (){
    return `code: ${this.id}, modelo: ${this.modelo}, marca: ${this.marca}, `+
    `color: ${this.color}, ubicacion: ${this.ubicacion}`;
}

BicicletaSchema.statics.allBici = function (cb) {
   return this.find({},cb);   
}

BicicletaSchema.statics.createInstance = function (codigo, modelo, marca, color, ubicacion) {
    return new this({
        codigo,
        modelo,
        marca,
        color,
        ubicacion
    })  
};

BicicletaSchema.statics.add = function (aBicicleta, cb) {
    return this.create(aBicicleta,cb);  
};

module.exports = mongoose.model('Bicicleta', BicicletaSchema);