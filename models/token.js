const mongoose = require('mongoose');
const schema = mongoose.Schema;

var TokenSchema = new schema({
    _userId:{ type: mongoose.Schema.Types.ObjectId, required: true, ref:'Usuario'},
    token:{ type: String, required: true},
    createdAt:{ type: Date, required: true, default: Date.now, expires: 4320}
});

module.exports = mongoose.model('Token', TokenSchema);