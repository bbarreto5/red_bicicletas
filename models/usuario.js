const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const schema = mongoose.Schema;
const Reserva = require('./reserva');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const  validarEmail = function (email) {
    let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
    return re.test(String(email).toLowerCase());
}

var UsuarioSchema = new schema({
    nombre: {
        type: String,
        trim: true,
        required:[true,"nombre requerido"]
    },
    email:{
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        validate: [validarEmail,'email no permitido'],
        match:  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/,
    },
    password:{
        type: String,
        required:[true, "password requerido"],
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false,
    }
});

UsuarioSchema.pre('save',function (next) {
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password,saltRounds);
    }
    next();
});

UsuarioSchema.methods.validPassword = function (pass) {
    return bcrypt.compareSync(pass,this.password);
}

UsuarioSchema.methods.reservar = function (idBicicleta, desde, hasta, cb) {
    let aRes = new Reserva({usuario: this._id, bicicleta: idBicicleta, desde, hasta});
    console.log(aRes);
    aRes.save(cb);
};

UsuarioSchema.plugin(uniqueValidator, { message:'El {PATH} YA EXISTE CON OTRO USUARIO' });

module.exports = mongoose.model('Usuario', UsuarioSchema);